package com.lalawaimai.plateform.http;

import com.lalawaimai.palteform.baselib.utils.NullUtil;

/**
 * 网址集合类
 * Created by WaterWood on 2018/5/31.
 */
public class WebUrl {
    private static final String WEB_URL_HEAD = "http://192.168.0.109/we7";//基础地址
    private static final String I = "1";

    public static final String GET_ORDER_LIST = getUrl("order", "takeout", "");//订单列表
    public static final String GET_ORDER_DETAIL = getUrl("order", "takeout", "detail");//订单详情

    private static String getUrl(String acStr, String opStr, String taStr) {
        if (NullUtil.isStringEmpty(taStr)) {
            return WEB_URL_HEAD + "/app/index.php?i=" + I + "&c=entry&ctrl=plateform&ac=" + acStr + "&op=" + opStr + "&do=api&m=we7_wmall";
        } else {
            return WEB_URL_HEAD + "/app/index.php?i=" + I + "&c=entry&ctrl=plateform&ac=" + acStr + "&op=" + opStr + "&ta=" + taStr + "&do=api&m=we7_wmall";
        }
    }
}
