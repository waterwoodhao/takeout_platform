package com.lalawaimai.plateform.order.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.order.fragment.MoreOrderFragment;
import java.util.ArrayList;
import java.util.List;

/**
 * 更多页面
 * Created by WaterWood on 2018/6/1.
 */
public class MoreOrderActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_back;
    private View status_bar;
    private TextView tv_title;
    private TabLayout order_tablayout;
    private ViewPager order_viewpager;
    private List<Fragment> fragmentList;
    private List<String> list_title;
    private FragmentPagerAdapter mAdapter;

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_more_order;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                finish();
                break;
        }
    }

    @Override
    protected void initWidget() {
        //初始化控件
        ll_back = findViewById(R.id.ll_back);
        ll_back.setOnClickListener(this);
        status_bar = findViewById(R.id.status_bar);
        setStatusBarSize(this,status_bar);
        tv_title = findViewById(R.id.tv_title);
        tv_title.setText("更多订单");
        order_tablayout = findViewById(R.id.order_tablayout);
        order_viewpager = findViewById(R.id.order_viewpager);
        //添加Tab
        TabLayout.Tab tab1 = order_tablayout.newTab();
        tab1.setText("已完成");
        order_tablayout.addTab(tab1);
        TabLayout.Tab tab2 = order_tablayout.newTab();
        tab2.setText("已取消");
        order_tablayout.addTab(tab2);
        TabLayout.Tab tab3 = order_tablayout.newTab();
        tab3.setText("退款单");
        order_tablayout.addTab(tab3);
        //初始化两个页面，并设置标记
        MoreOrderFragment moreOrderFragment1 = new MoreOrderFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("flag", 1);
        moreOrderFragment1.setArguments(bundle1);

        MoreOrderFragment moreOrderFragment2 = new MoreOrderFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putInt("flag", 2);
        moreOrderFragment2.setArguments(bundle2);

        MoreOrderFragment moreOrderFragment3 = new MoreOrderFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putInt("flag", 3);
        moreOrderFragment3.setArguments(bundle3);
        fragmentList = new ArrayList<>();
        fragmentList.add(moreOrderFragment1);
        fragmentList.add(moreOrderFragment2);
        fragmentList.add(moreOrderFragment3);
        list_title = new ArrayList<>();
        list_title.add("已完成");
        list_title.add("已取消");
        list_title.add("退款单");
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return list_title.get(position);
            }
        };
        order_viewpager.setAdapter(mAdapter);
        order_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        order_tablayout.setupWithViewPager(order_viewpager);
    }

    @Override
    protected void intiData() {

    }
}
