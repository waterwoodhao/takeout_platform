package com.lalawaimai.plateform.order.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderDetailBean;
import com.lalawaimai.plateform.http.WebUrl;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单详情页
 * Created by WaterWood on 2018/5/30.
 */
public class OrderDetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_title;
    private LinearLayout ll_back;
    private View status_bar;
    private String id;
    private TextView stroe_name;
    private TextView store_address;
    private TextView customer_info;
    private TextView customer_name;
    private TextView orderdetail_shopmobile;
    private TextView orderdetail_usermobile;
    private TextView send_time;
    private TextView number;
    private TextView orderdetail_mark;
    private TextView delivery_fee;
    private TextView box_fee;
    private TextView pack_fee;
    private TextView order_price;
    private TextView discount_fee;
    private TextView final_fee;
    private TextView order_sn;
    private TextView addtime;
    private TextView pay_type;
    private TextView assign_date;
    private TextView assign_time;
    private TextView instore_date;
    private TextView instore_time;
    private TextView takegoods_date;
    private TextView takegoods_time;
    private TextView success_date;
    private TextView success_time;
    private String status;
    private Button bt1;
    private Button bt2;
    private Button bt3;
    private Button bt4;
    private Button bt5;
    private Button bt6;
    private boolean isMore;

    @Override
    protected boolean initArgs(Bundle bundle) {
        id = bundle.getString("id");
        status = bundle.getString("status");
        isMore = bundle.getBoolean("isMore",false);
        if (NullUtil.isStringEmpty(id)){
            return false;
        }else{
            return true;
        }
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.activity_order_detail;
    }

    @Override
    protected void initWidget() {
        //初始化控件
        tv_title = findViewById(R.id.tv_title);
        ll_back = findViewById(R.id.ll_back);
        stroe_name = findViewById(R.id.stroe_name);
        store_address = findViewById(R.id.store_address);
        customer_info = findViewById(R.id.customer_info);
        customer_name = findViewById(R.id.customer_name);
        orderdetail_shopmobile = findViewById(R.id.orderdetail_shopmobile);
        orderdetail_usermobile = findViewById(R.id.orderdetail_usermobile);
        send_time = findViewById(R.id.send_time);
        number = findViewById(R.id.number);
        orderdetail_mark = findViewById(R.id.orderdetail_mark);
        delivery_fee = findViewById(R.id.delivery_fee);
        box_fee = findViewById(R.id.box_fee);
        pack_fee = findViewById(R.id.pack_fee);
        order_price = findViewById(R.id.order_price);
        discount_fee = findViewById(R.id.discount_fee);
        final_fee = findViewById(R.id.final_fee);
        order_sn = findViewById(R.id.order_sn);
        addtime = findViewById(R.id.addtime);
        pay_type = findViewById(R.id.pay_type);
        assign_date = findViewById(R.id.assign_date);
        assign_time = findViewById(R.id.assign_time);
        instore_date = findViewById(R.id.instore_date);
        instore_time = findViewById(R.id.instore_time);
        takegoods_date = findViewById(R.id.takegoods_date);
        takegoods_time = findViewById(R.id.takegoods_time);
        success_date = findViewById(R.id.success_date);
        success_time = findViewById(R.id.success_time);
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        bt3 = findViewById(R.id.bt3);
        bt4 = findViewById(R.id.bt4);
        bt5 = findViewById(R.id.bt5);
        bt6 = findViewById(R.id.bt6);
        //这里是对状态栏占位控件的设置，只要获取到控件调用这个方法就好。这是正常情况下都需要调用，但是如果是没有使用commonTitle的话，就不用这个了。
        status_bar = findViewById(R.id.status_bar);
        setStatusBarSize(this, status_bar);
        ll_back.setOnClickListener(this);
        tv_title.setText("订单详情");
        //这里处理一下按钮的显示
        if (!isMore) {
            if (status.equals("1")) {
                bt4.setVisibility(View.GONE);
                bt5.setVisibility(View.GONE);
                bt6.setVisibility(View.GONE);
                bt1.setText("取消");
                bt2.setText("通知商户");
                bt3.setText("确定接单");
            } else if (status.equals("2")) {
                bt4.setVisibility(View.GONE);
                bt5.setVisibility(View.GONE);
                bt6.setVisibility(View.GONE);
                bt1.setText("取消");
                bt2.setText("通知配送员");
                bt3.setText("调度");
            } else if (status.equals("3")) {
                bt4.setVisibility(View.GONE);
                bt5.setVisibility(View.GONE);
                bt6.setVisibility(View.GONE);
                bt1.setText("取消");
                bt2.setText("通知配送员");
                bt3.setText("调度");
            } else if (status.equals("4")) {
                bt5.setVisibility(View.GONE);
                bt6.setVisibility(View.GONE);
                bt1.setText("取消");
                bt2.setText("重置为代抢");
                bt3.setText("调度");
                bt4.setText("完成");
            } else if (status.equals("5")) {
                bt4.setVisibility(View.GONE);
                bt5.setVisibility(View.GONE);
                bt6.setVisibility(View.GONE);
                bt1.setText("已退款");
                bt2.setText("查询退款进度");
                bt3.setText("发起退款");
            }
        }else{
            bt1.setVisibility(View.GONE);
            bt2.setVisibility(View.GONE);
            bt3.setVisibility(View.GONE);
            bt4.setVisibility(View.GONE);
            bt5.setVisibility(View.GONE);
            bt6.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_back:
                finish();
                break;
        }
    }

    @Override
    protected void intiData() {
        String url = WebUrl.GET_ORDER_DETAIL;
        Class<?> clazz = OrderDetailBean.class;
        List<String> list = new ArrayList<>();
        list.add("id");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                OrderDetailBean orderDetailBean = (OrderDetailBean) responseObj;
                stroe_name.setText(orderDetailBean.getMessage().getData().getStore().getTitle());
                store_address.setText(orderDetailBean.getMessage().getData().getStore().getAddress());
                customer_info.setText(orderDetailBean.getMessage().getData().getAddress());
                customer_name.setText(orderDetailBean.getMessage().getData().getUsername()+orderDetailBean.getMessage().getData().getSex()+" "+orderDetailBean.getMessage().getData().getMobile());
                orderdetail_shopmobile.setText(orderDetailBean.getMessage().getData().getStore().getTelephone());
                orderdetail_usermobile.setText(orderDetailBean.getMessage().getData().getMobile());
                send_time.setText(orderDetailBean.getMessage().getData().getDeliverytime_cn());
                number.setText("#"+orderDetailBean.getMessage().getData().getSerial_sn());
                orderdetail_mark.setText(orderDetailBean.getMessage().getData().getNote());
                delivery_fee.setText("￥"+orderDetailBean.getMessage().getData().getDelivery_fee());
                box_fee.setText("￥"+orderDetailBean.getMessage().getData().getBox_price());
                pack_fee.setText("￥"+orderDetailBean.getMessage().getData().getPack_fee());
                order_price.setText("￥"+orderDetailBean.getMessage().getData().getTotal_fee());
                discount_fee.setText("￥"+orderDetailBean.getMessage().getData().getDiscount_fee());
                final_fee.setText("￥"+orderDetailBean.getMessage().getData().getFinal_fee());
                order_sn.setText(orderDetailBean.getMessage().getData().getOrdersn());
                addtime.setText(orderDetailBean.getMessage().getData().getAddtime_cn());
                pay_type.setText(orderDetailBean.getMessage().getData().getPay_type_cn());
                assign_date.setText(orderDetailBean.getMessage().getData().getDelivery_assign_time_cn().getDay());
                assign_time.setText(orderDetailBean.getMessage().getData().getDelivery_assign_time_cn().getTime());
                instore_date.setText(orderDetailBean.getMessage().getData().getDelivery_instore_time_cn().getDay());
                instore_time.setText(orderDetailBean.getMessage().getData().getDelivery_instore_time_cn().getTime());
                takegoods_date.setText(orderDetailBean.getMessage().getData().getDelivery_takegoods_time_cn().getDay());
                takegoods_time.setText(orderDetailBean.getMessage().getData().getDelivery_takegoods_time_cn().getTime());
                success_date.setText(orderDetailBean.getMessage().getData().getDelivery_success_time_cn().getDay());
                success_time.setText(orderDetailBean.getMessage().getData().getDelivery_success_time_cn().getTime());
            }

            @Override
            public void onFailure(Object reasonObj) {

            }
        }).requestWeb(url,clazz,list,id);
    }
}
