package com.lalawaimai.plateform.order.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.lalawaimai.palteform.baselib.base.BaseFragment;
import com.lalawaimai.palteform.baselib.http.DisposeDataListener;
import com.lalawaimai.palteform.baselib.http.OkHttpException;
import com.lalawaimai.palteform.baselib.http.RequestWebInfo;
import com.lalawaimai.palteform.baselib.utils.CommonUtil;
import com.lalawaimai.palteform.baselib.utils.NullUtil;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderListBean;
import com.lalawaimai.plateform.bean.OrderListEntity;
import com.lalawaimai.plateform.http.WebUrl;
import com.lalawaimai.plateform.order.adapter.WaitOrderAdapter;
import com.lalawaimai.plateform.order.activity.OrderDetailActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单列表内页
 * Created by WaterWood on 2018/5/30.
 */
public class WaitOrderFragment extends BaseFragment{

    private int flag;//各个内页区分
    private ListView lv_order;
    private WaitOrderAdapter waitOrderAdapter;
    private SmartRefreshLayout refreshLayout;
    private int page = 1;
    private String status;
    private List<OrderListEntity> list_order;//列表中的内容
    private final int MAX_PAGE = 3;//每页最大数量

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_wait_order;
    }

    @Override
    protected void initArgs(Bundle bundle) {
        flag = bundle.getInt("flag",0);
        if (flag == 1){
            status = "1";
        }else if (flag ==2){
            status ="2";
        }else if (flag == 3){
            status = "3";
        }else if (flag == 4){
            status = "4";
        }else if (flag == 5){
            status = "7";
        }
    }

    @Override
    protected void initWidget(View root) {
        //初始化控件
        lv_order = root.findViewById(R.id.lv_order);
        refreshLayout = root.findViewById(R.id.refreshLayout);
        //初始化布局
        list_order = new ArrayList<>();
        waitOrderAdapter = new WaitOrderAdapter(getActivity(),list_order);
        waitOrderAdapter.setType(status);
        lv_order.setAdapter(waitOrderAdapter);
        lv_order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(),OrderDetailActivity.class);
                intent.putExtra("id",list_order.get(position).getId());
                intent.putExtra("status",status);
                startActivity(intent);
            }
        });
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 1;
                getOrderList();
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                page++;
                getOrderList();
            }
        });
    }

    @Override
    protected void initData() {
        refreshLayout.autoRefresh();
    }

    private void getOrderList(){
        String url = WebUrl.GET_ORDER_LIST;
        Class<?> clazz = OrderListBean.class;
        List<String> list = new ArrayList<>();
        list.add("status");
        list.add("page");
        new RequestWebInfo(new DisposeDataListener() {
            @Override
            public void onSuccess(Object responseObj) {
                OrderListBean orderListBean = (OrderListBean) responseObj;
                if (page == 1){
                    refreshLayout.finishRefresh();
                    //刷新操作
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    list_order.clear();
                    if (!NullUtil.isListEmpty(list)){
                        //有东西
                        list_order.addAll(list);
                        //如果第一页就小于二十条，直接禁止加载更多
                        if (list.size()<MAX_PAGE){
                            refreshLayout.setEnableLoadMore(false);
                        }else{
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                    }else{
                        //没有东西，显示空页面，隐藏列表
                    }
                    waitOrderAdapter.notifyDataSetChanged();
                }else{
                    //加载更多
                    refreshLayout.finishLoadMore();
                    List<OrderListEntity> list = orderListBean.getMessage().getData().getOrder();
                    if (!NullUtil.isListEmpty(list)){
                        //有东西
                        list_order.addAll(list);
                        //如果这一页小于这么多，直接禁止加载更多
                        if (list.size()<MAX_PAGE){
                            refreshLayout.setEnableLoadMore(false);
                        }else{
                            if (!refreshLayout.isEnableLoadMore()) {
                                refreshLayout.setEnableLoadMore(true);
                            }
                        }
                        waitOrderAdapter.notifyDataSetChanged();
                    }else{
                        //没东西
                        CommonUtil.showToast(getActivity(),"已经加载到底了");
                        refreshLayout.setEnableLoadMore(false);
                    }
                }
            }

            @Override
            public void onFailure(Object reasonObj) {
                refreshLayout.finishRefresh();
                refreshLayout.finishLoadMore();
                OkHttpException okHttpException = (OkHttpException) reasonObj;
                String msg = okHttpException.getEmsg().toString();
                if (msg.contains("java.net.ConnectException")){
                    CommonUtil.showToast(getActivity(),"无法连接服务器，请检查您的网络");
                }
            }
        }).requestWeb(url,clazz,list,status,page+"");
    }
}
