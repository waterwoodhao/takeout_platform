package com.lalawaimai.plateform.order.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import com.lalawaimai.palteform.baselib.base.BaseMineAdapter;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.bean.OrderListEntity;
import java.util.List;

public class MoreOrderAdapter extends BaseMineAdapter<OrderListEntity> {

    public MoreOrderAdapter(Activity activity, List<OrderListEntity> list) {
        super(activity, list);
    }

    /**
     * 获取Holder的具体实现
     *
     * @return
     */
    @Override
    protected Object getHolderChild() {
        return new MineHolder();
    }

    /**
     * 获取item布局
     *
     * @return
     */
    @Override
    protected int getLayoutId() {
        return R.layout.item_wait_order;
    }

    /**
     * 初始化Holder中的控件
     *
     * @param convertView
     * @param object
     */
    @Override
    protected void initHolderWidge(View convertView, Object object) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.bottom_line = convertView.findViewById(R.id.bottom_line);
        mineHolder.number = convertView.findViewById(R.id.number);
        mineHolder.price = convertView.findViewById(R.id.price);
        mineHolder.order_get_shop = convertView.findViewById(R.id.order_get_shop);
        mineHolder.order_get_address = convertView.findViewById(R.id.order_get_address);
        mineHolder.order_send_address = convertView.findViewById(R.id.order_send_address);
        mineHolder.name_and_phone = convertView.findViewById(R.id.name_and_phone);
        mineHolder.note = convertView.findViewById(R.id.note);
        mineHolder.add_time = convertView.findViewById(R.id.add_time);
        mineHolder.delivery_time = convertView.findViewById(R.id.delivery_time);
        mineHolder.bt1 = convertView.findViewById(R.id.bt1);
        mineHolder.bt2 = convertView.findViewById(R.id.bt2);
        mineHolder.bt3 = convertView.findViewById(R.id.bt3);
        mineHolder.bt4 = convertView.findViewById(R.id.bt4);
        mineHolder.view_line = convertView.findViewById(R.id.view_line);
    }

    /**
     * 初始化Holder中的控件数据
     *
     * @param object
     */
    @Override
    protected void initHolderData(Object object, int position) {
        MineHolder mineHolder = (MineHolder) object;
        mineHolder.view_line.setVisibility(View.GONE);
        mineHolder.bt1.setVisibility(View.GONE);
        mineHolder.bt2.setVisibility(View.GONE);
        mineHolder.bt3.setVisibility(View.GONE);
        mineHolder.bt4.setVisibility(View.GONE);
        if (position == list.size() - 1) {
            mineHolder.bottom_line.setVisibility(View.GONE);
        } else {
            mineHolder.bottom_line.setVisibility(View.VISIBLE);
        }
        mineHolder.number.setText("#" + list.get(position).getSerial_sn());
        mineHolder.price.setText("￥" + list.get(position).getFinal_fee());
        mineHolder.order_get_shop.setText(list.get(position).getStore().getTitle());
        mineHolder.order_get_address.setText(list.get(position).getStore().getAddress());
        mineHolder.order_send_address.setText(list.get(position).getAddress());
        mineHolder.name_and_phone.setText(list.get(position).getUsername()+" "+list.get(position).getMobile());
        mineHolder.note.setText(list.get(position).getNote());
        mineHolder.add_time.setText(list.get(position).getAddtime_cn());
        mineHolder.delivery_time.setText(list.get(position).getDeliverytime_cn());
    }

    public class MineHolder {
        public View bottom_line;
        public TextView number;
        public TextView price;
        public TextView order_get_shop;
        public TextView order_get_address;
        public TextView order_send_address;
        public TextView name_and_phone;
        public TextView note;
        public TextView add_time;
        public TextView delivery_time;
        public TextView bt1;
        public TextView bt2;
        public TextView bt3;
        public TextView bt4;
        public View view_line;
    }
}
