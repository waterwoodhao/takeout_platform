package com.lalawaimai.plateform.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lalawaimai.palteform.baselib.base.BaseActivity;
import com.lalawaimai.palteform.baselib.base.BaseFragment;
import com.lalawaimai.plateform.R;
import com.lalawaimai.plateform.order.activity.MoreOrderActivity;
import com.lalawaimai.plateform.order.fragment.WaitOrderFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单页面
 * Created by WaterWood on 2018/5/30.
 */
public class OrderFragment extends BaseFragment implements View.OnClickListener {

    private TabLayout order_tablayout;
    private ViewPager order_viewpager;
    private List<Fragment> list_fragment;
    private FragmentPagerAdapter mAdapter;
    private List<String> list_title;
    private TextView tv_title;
    private View status_bar;
    private BaseActivity baseActivity;
    private LinearLayout ll_search;
    private LinearLayout ll_more;

    @Override
    protected int getContentLayoutId() {
        return R.layout.fragment_order;
    }

    @Override
    protected void initWidget(View root) {
        //初始化控件
        order_tablayout = root.findViewById(R.id.order_tablayout);
        order_viewpager = root.findViewById(R.id.order_viewpager);
        tv_title = root.findViewById(R.id.tv_title);
        tv_title.setText("待接单列表");
        ll_search = root.findViewById(R.id.ll_search);
        ll_more = root.findViewById(R.id.ll_more);
        ll_search.setOnClickListener(this);
        ll_more.setOnClickListener(this);
        //这里是对状态栏占位控件的设置，只要获取到控件调用这个方法就好。这是正常情况下都需要调用，但是如果是没有使用commonTitle的话，就不用这个了。
        status_bar = root.findViewById(R.id.status_bar);
        baseActivity = (BaseActivity) getActivity();
        if (baseActivity != null){
            baseActivity.setStatusBarSize(getActivity(),status_bar);
        }
        //添加Tab
        TabLayout.Tab tab1 = order_tablayout.newTab();
        tab1.setText("待接单");
        order_tablayout.addTab(tab1);
        TabLayout.Tab tab2 = order_tablayout.newTab();
        tab2.setText("已确认");
        order_tablayout.addTab(tab2);
        TabLayout.Tab tab3 = order_tablayout.newTab();
        tab3.setText("待配送");
        order_tablayout.addTab(tab3);
        TabLayout.Tab tab4 = order_tablayout.newTab();
        tab4.setText("配送中");
        order_tablayout.addTab(tab4);
        TabLayout.Tab tab5 = order_tablayout.newTab();
        tab5.setText("退款单");
        order_tablayout.addTab(tab5);
        //初始化五个页面，并设置标记
        WaitOrderFragment waitOrderFragment1 = new WaitOrderFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putInt("flag", 1);
        waitOrderFragment1.setArguments(bundle1);

        WaitOrderFragment waitOrderFragment2 = new WaitOrderFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putInt("flag", 2);
        waitOrderFragment2.setArguments(bundle2);

        WaitOrderFragment waitOrderFragment3 = new WaitOrderFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putInt("flag", 3);
        waitOrderFragment3.setArguments(bundle3);

        WaitOrderFragment waitOrderFragment4 = new WaitOrderFragment();
        Bundle bundle4 = new Bundle();
        bundle4.putInt("flag", 4);
        waitOrderFragment4.setArguments(bundle4);

        WaitOrderFragment waitOrderFragment5 = new WaitOrderFragment();
        Bundle bundle5 = new Bundle();
        bundle5.putInt("flag", 5);
        waitOrderFragment5.setArguments(bundle5);
        //添加页面
        list_fragment = new ArrayList<>();
        list_fragment.add(waitOrderFragment1);
        list_fragment.add(waitOrderFragment2);
        list_fragment.add(waitOrderFragment3);
        list_fragment.add(waitOrderFragment4);
        list_fragment.add(waitOrderFragment5);
        list_title = new ArrayList<>();
        list_title.add("待接单");
        list_title.add("已确认");
        list_title.add("待配送");
        list_title.add("配送中");
        list_title.add("退款单");
        mAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return list_fragment.get(position);
            }

            @Override
            public int getCount() {
                return list_fragment.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return list_title.get(position);
            }
        };
        order_viewpager.setAdapter(mAdapter);
        order_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        tv_title.setText("待接单列表");
                        break;
                    case 1:
                        tv_title.setText("已确认列表");
                        break;
                    case 2:
                        tv_title.setText("待配送列表");
                        break;
                    case 3:
                        tv_title.setText("配送中列表");
                        break;
                    case 4:
                        tv_title.setText("退款单列表");
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        order_tablayout.setupWithViewPager(order_viewpager);
    }

    @Override
    protected void initData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_search:
                break;
            case R.id.ll_more:
                Intent intent = new Intent(context, MoreOrderActivity.class);
                startActivity(intent);
                break;
        }
    }
}
