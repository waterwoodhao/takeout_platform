package com.lalawaimai.plateform.bean;

/**
 * 订单列表实体类--列表项
 * Created by WaterWood on 2018/5/31.
 */
public class OrderListEntity {
    private String id;
    private String uniacid;
    private String agentid;
    private String sid;
    private String serial_sn;
    private String username;
    private String mobile;
    private String address;
    private String note;
    private String delivery_day;
    private String delivery_time;
    private String status;
    private String final_fee;
    private String addtime;
    private StoreBean store;
    private String addtime_cn;
    private String deliverytime_cn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUniacid() {
        return uniacid;
    }

    public void setUniacid(String uniacid) {
        this.uniacid = uniacid;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSerial_sn() {
        return serial_sn;
    }

    public void setSerial_sn(String serial_sn) {
        this.serial_sn = serial_sn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDelivery_day() {
        return delivery_day;
    }

    public void setDelivery_day(String delivery_day) {
        this.delivery_day = delivery_day;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinal_fee() {
        return final_fee;
    }

    public void setFinal_fee(String final_fee) {
        this.final_fee = final_fee;
    }

    public String getAddtime() {
        return addtime;
    }

    public void setAddtime(String addtime) {
        this.addtime = addtime;
    }

    public StoreBean getStore() {
        return store;
    }

    public void setStore(StoreBean store) {
        this.store = store;
    }

    public String getAddtime_cn() {
        return addtime_cn;
    }

    public void setAddtime_cn(String addtime_cn) {
        this.addtime_cn = addtime_cn;
    }

    public String getDeliverytime_cn() {
        return deliverytime_cn;
    }

    public void setDeliverytime_cn(String deliverytime_cn) {
        this.deliverytime_cn = deliverytime_cn;
    }

    public static class StoreBean {

        private String id;
        private String title;
        private String address;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
