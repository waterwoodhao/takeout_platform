package com.lalawaimai.palteform.baselib.utils;

import android.content.Context;
import android.util.TypedValue;
import android.widget.Toast;

/**
 * 普通常用工具类
 * Created by WaterWood on 2018/5/29.
 */
public class CommonUtil {
    /**
     * 显示土司
     *
     * @param context
     * @param msg
     */
    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取状态栏高度——方法1
     */
    public static int getStateBarHeight(Context context) {
        int statusBarHeight1 = -1;
        //获取status_bar_height资源的ID
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            //根据资源ID获取响应的尺寸值
            statusBarHeight1 = context.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight1;
    }

    /**
     * dp转px
     * @param dpval
     * @return
     */
    public static int dp2px(Context context,int dpval){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dpval,context.getResources().getDisplayMetrics());
    }
}
