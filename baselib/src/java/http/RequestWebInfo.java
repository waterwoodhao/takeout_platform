package com.lalawaimai.palteform.baselib.http;

import android.widget.Toast;

import java.util.List;

/**
 * 请求网络的主类
 * Created by WaterWood on 2018/5/29.
 */
public class RequestWebInfo {

    private DisposeDataListener disposeDataListener;
    public static String jsonInfo;//方便我看json数据

    public RequestWebInfo(DisposeDataListener disposeDataListener) {
        this.disposeDataListener = disposeDataListener;
    }

    /**
     * 请求类
     *
     * @param url
     * @param clazz
     * @param parameters
     */
    public void requestWeb(String url, Class<?> clazz, List<String> list, String... parameters) {
        //封装参数信息
        RequestParams requestParams = new RequestParams();
        for (int i = 0; i < parameters.length; i++) {
            requestParams.put(list.get(i), parameters[i]);
        }
        CommonOkHttpClient.sendRequest(CommonRequest.createPostRequest(url, requestParams), new CommonJsonCallback(new DisposeDataHandle(disposeDataListener, clazz)));
    }
}
